class DataModel {
  String category = '';
  String description = '';

  DataModel({required this.category, required this.description});

  factory DataModel.fromJson(Map<String, dynamic> json) {
    return DataModel(
        category: json['Category'], description: json['Description']);
  }
}
