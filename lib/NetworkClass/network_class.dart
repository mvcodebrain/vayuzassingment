import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'network_response.dart';
import 'web_url.dart';

class NetworkClass {
  String endUrl = "";
  NetworkResponse networkResponse = NetworkResponse();
  int requestCode = 0;
  Map<String, String>? jsonBody;
  AlertDialog? alertDialog;
  bool isShowing = false;

  NetworkClass(String url, NetworkResponse networkResponse, int requestCode) {
    this.endUrl = url;
    this.networkResponse = networkResponse;
    this.requestCode = requestCode;
  }

  Future<void> callGetService(BuildContext context, bool showLoader) async {
    try {
      if (showLoader) {
        isShowing = true;
        showLoaderDialog(context);
      }
      log("GGURL---->" + BASE_URL + endUrl);

      var url = Uri.parse(BASE_URL + endUrl);
      if (!kReleaseMode) log("URL---->" + endUrl);
      final response = await http.get(url);
      if (!kReleaseMode) log("Response---->");
      if (!kReleaseMode) {
        log("StatusCode---->" + response.statusCode.toString());
      }

      if (response.statusCode >= 200 && response.statusCode <= 299) {
        if (showLoader) {
          if (alertDialog != null && isShowing) {
            isShowing = false;
            Navigator.pop(context);
          }
        }
        networkResponse.onResponse(
            requestCode: requestCode, response: response.body.toString());
      } else {
        if (showLoader) {
          if (alertDialog != null && isShowing) {
            isShowing = false;
            Navigator.pop(context);
          }
        }
        networkResponse.onError(
            requestCode: requestCode, response: response.body.toString());
      }
    } on SocketException catch (e) {
      Navigator.pop(context);
      switch (e.osError!.errorCode) {
        case 7:
          showToast('No Internet Connection!');
          break;
        case 111:
          showToast('Unable to connect to server!');
          break;
      }
      log('Socket Exception thrown --> $e');
    } on TimeoutException catch (e) {
      Navigator.pop(context);
      showToast('Please! Try Again');
      log('TimeoutException thrown --> $e');
    } catch (err) {
      Navigator.pop(context);
      log('Error While Making network call -> $err');
    }
  }

  void showLoaderDialog(BuildContext context) {
    if (alertDialog != null && isShowing) {
      isShowing = false;
      Navigator.pop(context);
    }
    alertDialog = AlertDialog(
      elevation: 0,
      backgroundColor: Colors.white.withOpacity(0),
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CircularProgressIndicator(),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.white.withOpacity(0),
      context: context,
      builder: (BuildContext context) {
        return alertDialog!;
      },
    );
  }

  void showToast(String message) {
    Fluttertoast.showToast(
        backgroundColor: Colors.orange,
        msg: "  $message!  ",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        textColor: Colors.white,
        fontSize: 14);
  }
}
