import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:list_assign/NetworkClass/network_class.dart';
import 'package:list_assign/NetworkClass/network_response.dart';
import 'package:list_assign/NetworkClass/web_url.dart';
import 'package:http/http.dart' as http;
import 'package:list_assign/model/data_model.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> implements NetworkResponse {
  ScrollController? _controller;
  List<DataModel> dataList = [];
  bool isData = false;
  String noData = '';
  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      init();
    });
    super.initState();
  }

  init() async {
    callGetListApi();
    _controller = ScrollController();
    
    _controller!.addListener(() {
      if (_controller!.position.pixels ==
          _controller!.position.maxScrollExtent) {
        callGetListApi();
      }
    });
  }

  @override
  void dispose() {
    _controller!.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: listWidget(),
      ),
    );
  }

  Widget listWidget() {
    return Container(
      child: ListView.builder(
          itemCount: dataList.length,
          itemBuilder: (context, index) => listItem(index)),
    );
  }

  Widget listItem(int index) {
    var item = dataList[index];
    return Card(
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(item.category),
            Text(
              item.description,
              style: TextStyle(color: Colors.grey, fontSize: 12),
            ),
          ],
        ),
      ),
    );
  }

  void callGetListApi() async {
    NetworkClass(GET_LIST, this, REQ_GET_LIST).callGetService(context, true);

    // var url = Uri.parse(BASE_URL + GET_LIST);
    // log(url.toString());
    // try {
    //   var response = await http.get(url);

    //   log(response.body);
    // } catch (e) {
    //   log(e.toString());
    // }
  }

  @override
  void onError({Key? key, int? requestCode, String? response}) {
    // TODO: implement onError

    switch (requestCode) {
      case REQ_GET_LIST:
        log('message_ERROR======>  $response');

        setState(() {
          isData = false;
          noData = 'No data found!';
        });
    }
  }

  @override
  void onResponse({Key? key, int? requestCode, String? response}) {
    // TODO: implement onResponse
    try {
      switch (requestCode) {
        case REQ_GET_LIST:
          log('message_SUCCESS======>  $response');
          var map = jsonDecode(response!);

          var _listData = map['entries'] as List;
          setState(() {
            dataList.clear();
            var list = _listData.map((e) => DataModel.fromJson(e)).toList();
            dataList.addAll(list);

            if (dataList.length > 0) {
              isData = true;
              noData = '';
            } else {
              isData = false;
              noData = 'No data found!';
            }
          });
          break;
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
